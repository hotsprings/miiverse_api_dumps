# miiverse_api_dumps
These are my dumps of miiverse's api server before it shutdown. These files have been edited to remove sensitive info such as usernames.

files and their respective server urls<br>
<br>
<h2>Discovery server endpoint</h2>
<br>
discovery.xml = /v1/endpoint<br>
<br>
<h2>Olive server endpoint</h2>
<br>
communities.xml = /v1/communities<br>
communities_posts.xml = /v1/communities/"community_id"/posts<br>
people.xml = /v1/people<br>
topics.xml = /v1/topics
